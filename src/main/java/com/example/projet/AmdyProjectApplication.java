package com.example.projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmdyProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmdyProjectApplication.class, args);
	}

}
