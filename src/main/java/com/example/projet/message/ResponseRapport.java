package com.example.projet.message;

import lombok.AllArgsConstructor;
import lombok.Data;



@Data @AllArgsConstructor 
public class ResponseRapport {

	  private String name;
	  private String url;
	  private String type;
	  private long size;
	  
	
}
