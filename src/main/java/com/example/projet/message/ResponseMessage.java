package com.example.projet.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data @AllArgsConstructor 

public class ResponseMessage {

	  private String message;
}
