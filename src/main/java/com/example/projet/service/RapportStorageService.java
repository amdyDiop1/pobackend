package com.example.projet.service;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.projet.dao.RapportRepository;
import com.example.projet.entities.Rapport;

import org.springframework.util.StringUtils;

@Service
public class RapportStorageService {
  
	@Autowired
	private RapportRepository rapportRepository;
	
	  public Rapport store(MultipartFile file) throws IOException {
		    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		   Rapport rapportDB = new Rapport(fileName, file.getContentType(),file.getBytes());

		    return rapportRepository.save(rapportDB);
		  }

		  public Rapport getFile(String id) {
		    return rapportRepository.findById(id).get();
		  }
		  
		  public Stream<Rapport> getAllFiles() {
		    return rapportRepository.findAll().stream();
		  }
	
}
